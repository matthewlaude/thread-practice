package com.example.mvvmcolorpractice.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvvmcolorpractice.model.ButtonRepo
import kotlinx.coroutines.launch

class ButtonViewModel : ViewModel() {

    private val repo = ButtonRepo

    private val _randomColor = MutableLiveData<Int>()
    val randomColor : LiveData<Int> get() = _randomColor

    fun getRandomColor() {
        viewModelScope.launch {
            val randomColor = repo.getRandomColor()
            _randomColor.value = randomColor
        }
    }
}