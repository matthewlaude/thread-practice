package com.example.mvvmcolorpractice.model

import com.example.mvvmcolorpractice.model.remote.ButtonApi
import com.example.mvvmcolorpractice.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object ButtonRepo {

    private val buttonApi = object : ButtonApi {
        override suspend fun getRandomColor(): Int {
            return randomColor
        }
    }

    suspend fun getRandomColor() : Int = withContext(Dispatchers.IO) {
        delay(2000)
        buttonApi.getRandomColor()
    }
}