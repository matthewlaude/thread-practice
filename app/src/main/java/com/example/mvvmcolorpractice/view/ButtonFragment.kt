package com.example.mvvmcolorpractice.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mvvmcolorpractice.databinding.FragmentButtonBinding
import com.example.mvvmcolorpractice.viewmodel.ButtonViewModel

class ButtonFragment : Fragment() {

    private var _binding: FragmentButtonBinding? = null
    private val binding get() = _binding!!
    private val buttonViewModel by viewModels<ButtonViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentButtonBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonViewModel.randomColor.observe(viewLifecycleOwner) { randomColor ->
//            binding.tvRgb.setBackgroundColor(randomColor)
            val btnNextFragment = ButtonFragmentDirections.actionButtonFragment3ToColorFragment(randomColor)
            findNavController().navigate(btnNextFragment)
        }
        binding.btnRandom.setOnClickListener {
            buttonViewModel.getRandomColor()
        }

//        binding.btnNextFragment.setOnClickListener {
//            findNavController().navigate(R.id.action_buttonFragment3_to_colorFragment)
//        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}