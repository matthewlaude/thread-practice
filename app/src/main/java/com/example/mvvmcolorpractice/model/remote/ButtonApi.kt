package com.example.mvvmcolorpractice.model.remote

interface ButtonApi {

    suspend fun getRandomColor() : Int
}